from os import getenv

from dotenv import load_dotenv

load_dotenv()


class Var:
    API_ID = int(getenv("API_ID"))
    API_HASH = str(getenv("API_HASH"))
    BOT_TOKEN = str(getenv("BOT_TOKEN"))
    SESSION_NAME = str(getenv("SESSION_NAME", "AviStreamBot"))
    SLEEP_THRESHOLD = int(getenv("SLEEP_THRESHOLD", "60"))
    WORKERS = int(getenv("WORKERS", "4"))

    OWNER_ID = int(getenv("OWNER_ID", "797848243"))
    BIN_CHANNEL = int(getenv("BIN_CHANNEL"))

    BIND_ADDRESS = str(getenv("BIND_ADDRESS", "0.0.0.0"))
    BIND_PORT = int(getenv("BIND_PORT", 8080))
    FQDN = str(getenv("FQDN", f"http://{BIND_ADDRESS}"))

    DATABASE_URL = str(getenv("DATABASE_URL"))
