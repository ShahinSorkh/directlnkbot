import asyncio
import importlib
import logging
from pathlib import Path

from aiohttp import web
from pyrogram import idle

from .bot import StreamBot
from .server import web_server
from .vars import Var

logger = logging.getLogger(__name__)


async def start_services():
    await StreamBot.start()
    logger.info("bot initialized successfully")

    for name in Path(__file__).parent.glob("bot/plugins/*.py"):
        with open(name) as fp:
            plugin_name = Path(fp.name).stem
            importlib.import_module(f"web_streamer.bot.plugins.{plugin_name}")
            logger.info("imported %s", plugin_name)

    app = web.AppRunner(await web_server())
    await app.setup()
    await web.TCPSite(app, Var.BIND_ADDRESS, Var.BIND_PORT).start()
    logger.info(
        "web server started successfully. listening on %s:%s",
        Var.BIND_ADDRESS,
        Var.BIND_PORT,
    )

    await idle()


if __name__ == "__main__":
    logging.basicConfig(
        format="[%(name)s] %(levelname)s - %(message)s",
        level=logging.WARNING,
    )
    logging.getLogger("web_streamer").setLevel(logging.INFO)
    logger.setLevel(logging.INFO)

    try:
        asyncio.run(start_services())
    except KeyboardInterrupt:
        logger.info("exiting...")
