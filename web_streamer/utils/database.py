from datetime import date
from typing import Optional

import motor.motor_asyncio

from ..vars import Var


class User:
    def __init__(self, id_: int, banned: bool, join_date: date) -> None:
        self.id = id_
        self.banned = banned
        self.join_date = join_date

    @classmethod
    def new(cls, id_: int) -> "User":
        return cls(id_, False, date.today())

    @classmethod
    def from_dict(cls, user_dict: dict) -> "User":
        banned = user_dict["banned"] if "banned" in user_dict else False
        return cls(user_dict["id"], banned, user_dict["join_date"])


class Database:
    def __init__(self, uri, database_name):
        self._client = motor.motor_asyncio.AsyncIOMotorClient(uri)
        self._db = self._client[database_name]
        self._users = self._db.users

    async def add_user(self, id_):
        user = User.new(id_)
        await self._users.insert_one(user)

    async def user_exists(self, id_):
        return await self._users.find_one({"id": int(id_)}, {"id": 1}) is not None

    async def count_users(self):
        return await self._users.count_documents({})

    async def get_all_users(self):
        return self._users.find({})

    async def delete_user(self, id_):
        await self._users.delete_many({"id": int(id_)})

    async def find_user(self, id_) -> Optional[User]:
        user_dict = await self._users.find_one({"id": int(id_)})
        return User.from_dict(user_dict) if user_dict else None

    async def ban_user(self, id_):
        await self._users.update_one({"id": int(id_)}, {"$set": {"banned": True}})

    async def unban_user(self, id_):
        await self._users.update_one({"id": int(id_)}, {"$set": {"banned": False}})


db = Database(Var.DATABASE_URL, Var.SESSION_NAME)
