from pyrogram import Client, filters
from pyrogram.enums import ParseMode
from pyrogram.types import Message

from web_streamer.bot import StreamBot
from web_streamer.utils.database import Database
from web_streamer.vars import Var

db = Database(Var.DATABASE_URL, Var.SESSION_NAME)


@StreamBot.on_message(
    filters.command("status") & filters.private & filters.user(Var.OWNER_ID)
)
async def sts(c: Client, m: Message):
    total_users = await db.count_users()
    await m.reply_text(
        text=f"**Total Users in DB:** `{total_users}`",
        parse_mode=ParseMode.MARKDOWN,
        quote=True,
    )
