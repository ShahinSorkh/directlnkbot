import urllib.parse

from pyrogram import Client, filters
from pyrogram.enums import ParseMode
from pyrogram.types import InlineKeyboardButton, InlineKeyboardMarkup, Message

from web_streamer.bot import StreamBot
from web_streamer.bot.utils import prevent_banned_user
from web_streamer.utils.human_readable import humanbytes
from web_streamer.vars import Var

START_TEXT = """
__Hey,__ [{text}]({url})\n
__I'm Telegram Files Streaming Bot as well Direct Links Generate__\n
__Click on Help to get more information__\n
**WARNING** __Porn content leads to permanent ban.__
"""

HELP_TEXT = """
- __Send me any file/media from telegram.__
- __I will provide external direct download link!__
\n**WARNING** __Porn content leads to permanent ban.__
"""

ABOUT_TEXT = """
**My name:** DirectLnk
**Version:** 4.0.0
**Source:** https://gitlab.com/ShahinSorkh/directlnkbot
**Maintainer:** @ShahinSorkh\n
__Do not disturb. File any issue on [GitLab](https://gitlab.com/ShahinSorkh/directlnkbot/-/issues) only.__
"""

START_BUTTONS = InlineKeyboardMarkup(
    [
        [
            InlineKeyboardButton("Help", callback_data="help"),
            InlineKeyboardButton("About", callback_data="about"),
        ]
    ]
)
HELP_BUTTONS = InlineKeyboardMarkup(
    [
        [
            InlineKeyboardButton("Home", callback_data="home"),
            InlineKeyboardButton("About", callback_data="about"),
        ]
    ]
)
ABOUT_BUTTONS = InlineKeyboardMarkup(
    [
        [
            InlineKeyboardButton("Home", callback_data="home"),
            InlineKeyboardButton("Help", callback_data="help"),
        ]
    ]
)


@StreamBot.on_callback_query()
@prevent_banned_user
async def cb_data(c: Client, m: Message):
    if m.data == "home":
        await m.message.edit_text(
            text=START_TEXT.format(m.from_user.mention),
            parse_mode=ParseMode.DEFAULT,
            disable_web_page_preview=True,
            reply_markup=START_BUTTONS,
        )
    elif m.data == "help":
        await m.message.edit_text(
            text=HELP_TEXT,
            parse_mode=ParseMode.MARKDOWN,
            disable_web_page_preview=True,
            reply_markup=HELP_BUTTONS,
        )
    elif m.data == "about":
        await m.message.edit_text(
            text=ABOUT_TEXT,
            parse_mode=ParseMode.MARKDOWN,
            disable_web_page_preview=True,
            reply_markup=ABOUT_BUTTONS,
        )


def get_media_file_size(m: Message):
    media = m.video or m.audio or m.document
    return media.file_size if media and media.file_size else None


def get_media_file_name(m: Message):
    media = m.video or m.document or m.audio
    return (
        urllib.parse.quote_plus(media.file_name) if media and media.file_name else None
    )


@StreamBot.on_message(filters.command("start") & filters.private)
@prevent_banned_user
async def start(c: Client, m: Message):
    usr_cmd = m.text.split("_")[-1]
    if usr_cmd == "/start":
        await m.reply_text(
            text=START_TEXT.format(
                text=m.from_user.mention.text,
                url=m.from_user.mention.url,
            ),
            parse_mode=ParseMode.MARKDOWN,
            disable_web_page_preview=True,
            reply_markup=START_BUTTONS,
        )
        return

    bin_msg = await c.get_messages(chat_id=Var.BIN_CHANNEL, message_ids=int(usr_cmd))
    file_name = get_media_file_name(bin_msg)
    file_size = humanbytes(get_media_file_size(bin_msg))

    stream_link = f"{Var.FQDN}/{bin_msg.message_id}/{file_name}"
    msg_text = (
        f"**File name:** __{file_name}__\n"
        f"**File size:** __{file_size}__\n"
        f"**Download:** {stream_link}\n"
        "\n__Note: Link expires in 24 hours__"
    )

    await m.reply_text(
        text=msg_text,
        parse_mode=ParseMode.MARKDOWN,
        disable_web_page_preview=True,
        reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton("Download now", url=stream_link)]]
        ),
    )


@StreamBot.on_message(filters.command("about") & filters.private)
@prevent_banned_user
async def start(c: Client, m: Message):
    await m.reply_text(
        text=ABOUT_TEXT.format(m.from_user.mention),
        parse_mode=ParseMode.MARKDOWN,
        disable_web_page_preview=True,
        reply_markup=ABOUT_BUTTONS,
    )


@StreamBot.on_message(filters.command("help") & filters.private)
@prevent_banned_user
async def help_handler(c: Client, m: Message):
    await m.reply_text(
        text=HELP_TEXT,
        parse_mode=ParseMode.MARKDOWN,
        disable_web_page_preview=True,
        reply_markup=HELP_BUTTONS,
    )
