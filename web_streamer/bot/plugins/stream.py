import asyncio
import logging
import urllib.parse

from pyrogram import Client, filters
from pyrogram.enums import ParseMode
from pyrogram.errors import FloodWait
from pyrogram.types import InlineKeyboardButton, InlineKeyboardMarkup, Message

from web_streamer.bot import StreamBot
from web_streamer.bot.utils import prevent_banned_user
from web_streamer.utils.database import db
from web_streamer.utils.human_readable import humanbytes
from web_streamer.vars import Var

logger = logging.getLogger(__name__)


def get_media_file_size(m):
    media = m.video or m.audio or m.document
    return media.file_size if media and media.file_size else None


def get_media_file_name(m):
    media = m.video or m.document or m.audio
    return media.file_name if media and media.file_name else None


@StreamBot.on_message(
    filters.private & (filters.document | filters.video | filters.audio), group=4
)
@prevent_banned_user
async def private_receive_handler(c: Client, m: Message):
    try:
        bin_msg = await m.forward(chat_id=Var.BIN_CHANNEL)
        file_name = get_media_file_name(m)
        file_size = humanbytes(get_media_file_size(m))
        stream_link = f"{Var.FQDN}/{bin_msg.id}/" + urllib.parse.quote_plus(file_name)

        msg_text = (
            f"**File name:** __{file_name}__\n"
            f"**File size:** __{file_size}__\n"
            f"**Download:** __{stream_link}__\n"
            "\n__Note: This link is permanent and does not expire__"
        )

        await bin_msg.reply_text(
            text=(
                f"**Requested by:** {m.from_user.mention}\n"
                f"**User id:** `{m.from_user.id}`\n"
                f"**Download link:** {stream_link}"
            ),
            disable_web_page_preview=True,
            parse_mode=ParseMode.DEFAULT,
            quote=True,
        )
        await m.reply_text(
            text=msg_text,
            parse_mode=ParseMode.MARKDOWN,
            disable_web_page_preview=True,
            reply_markup=InlineKeyboardMarkup(
                [[InlineKeyboardButton("Download now", url=stream_link)]]
            ),
            quote=True,
        )
    except FloodWait as e:
        logger.warning("Sleeping for %fs", e.x)
        await asyncio.sleep(e.x)
        await c.send_message(
            chat_id=Var.BIN_CHANNEL,
            text=f"Got FloodWait of {e.x}s from {m.from_user.mention}\n\n**User ID:** `{m.from_user.id}`",
            disable_web_page_preview=True,
            parse_mode=ParseMode.DEFAULT,
        )
