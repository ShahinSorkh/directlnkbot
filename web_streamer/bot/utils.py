from functools import wraps

from pyrogram import Client
from pyrogram.enums import ParseMode
from pyrogram.types import Message

from ..utils.database import db
from ..vars import Var


def prevent_banned_user(f):
    @wraps(f)
    async def wrapper(c: Client, m: Message):
        user = await db.find_user(m.from_user.id)
        if user is None:
            await db.add_user(m.from_user.id)
            await c.send_message(
                chat_id=Var.BIN_CHANNEL,
                text=f"**New user joined **\n\n[{m.from_user.first_name} {m.from_user.last_name}](tg://user?id={m.from_user.id})",
                parse_mode=ParseMode.MARKDOWN,
                disable_web_page_preview=True,
            )
        elif user.id != Var.OWNER_ID and user.banned:
            await c.send_message(
                chat_id=m.chat.id,
                text="__You are banned to use this bot.__ Fuck off",
                parse_mode=ParseMode.MARKDOWN,
                disable_web_page_preview=True,
            )
            return
        return await f(c, m)

    return wrapper
